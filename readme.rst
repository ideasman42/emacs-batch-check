
#################
Emacs Batch Check
#################

**WARNING: this script is currently experimental, it may be further improved for general use in the future.**

This is a simple utility to run source code checking tools on emacs-lisp scripts.

Currently, the following checkers are used:

- Byte code compilation.
- Check-doc.
- Package Lint.
- ReLint (for ``regex``).


Usage
=====

Recursively check all ``*.el`` files.

.. code:: sh

   emacs-batch-check .


Check two files.

.. code:: sh

   emacs-batch-check example-file.el another-file.el


Installation
============

Since this uses a single self-contained script, the following steps are sufficient.

.. code:: sh

   git clone https://gitlab.com/ideasman42/emacs-batch-check.git
   ln -s emacs-batch-check/emacs-batch-check ~/.local/bin/


Now ``emacs-batch-check`` can be run to check files.


TODO
====

- ``--help`` message.
- Investigate supporting other checking tools.
